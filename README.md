Tracie dashboard
================

A web dashboard for the results of the tracie trace-based Mesa CI.

The dashboard is built using Django.

Get dashboard
-------------

    $ git clone https://gitlab.freedesktop.org/gfx-ci/tracie/tracie_dashboard

Activate and prepare virtualenv
-------------------------------

    $ python3 -m venv tracie-dashboard-venv
    $ source tracie-dashboard-venv/bin/activate
    $ pip install -r tracie_dashboard/requirements.txt

Instructions below assume that the virtulenv is active.

Prepare dashboard
-----------------

Edit `tracie_dashboard/tracie_dashboard/local_settings.py` and add the following
(after filling in sensible values for your setup):

```
SECRET_KEY = 'django_secret_key'
GITLAB_TOKEN = 'token_to_access_gitlab_api'
ALLOWED_HOSTS = [...]
```

You can get a django secret key with:

    $ python3 -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key());'

A gitlab access token can be generated from your gitlab.freedesktop.org profile,
in the "Personal Access Token" section.

In tracie_dashboard do:

    tracie_dashboard$ python3 manage.py makemigrations
    tracie_dashboard$ python3 manage.py migrate

Run dashboard
-------------

For a test run of the dashboard (note that first run may take some time while
data is downloaded from gitlab.freedesktop.org and cached):

    tracie_dashboard$ python3 manage.py runserver

Run the tests with:

    tracie_dashboard$ python3 manage.py test
