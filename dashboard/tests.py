from django.test import TestCase, Client
from dashboard.models import PipelineResult, TraceResult
from dashboard.views import ci

class FakeCI:
    def __init__(self):
        self.pipelines = []
        self.jobs = []

        # Monkey patch ci module used by dashboard.views
        ci.get_pipeline_ids = self.get_pipeline_ids
        ci.get_pipeline = self.get_pipeline
        ci.get_test_job = self.get_test_job

    def add_pipeline(self, pid, status, test_job_id, mesa_push):
        p = ci.Pipeline()
        p.id = pid;
        p.status = status
        p.mesa_sha = "0a1b2c3d" + str(pid)
        p.test_job_id = test_job_id
        p.triggered_by_mesa_push = mesa_push

        self.pipelines.append(p)

    def add_job(self, jid, status, trace_results):
        j = ci.Job()
        j.id = jid
        j.status = status
        j.trace_results = trace_results
        self.jobs.append(j)

    def get_pipeline_ids(self):
        return [p.id for p in self.pipelines]

    def get_pipeline(self, pid):
        return next(p for p in self.pipelines if p.id == pid)

    def get_test_job(self, jid):
        return next(j for j in self.jobs if j.id == jid)

class DashboardTestCase(TestCase):
    def _assertLandingContainsPipeline(self, response, pipeline):
        pipeline_link = "/dashboard/pipeline/" + str(pipeline.id) + "/"
        self.assertContains(response, pipeline.mesa_sha)
        self.assertContains(response, pipeline_link)
        self.assertContains(response, ci.pipeline_url(pipeline.id))

    def _assertLandingNotContainsPipeline(self, response, pipeline):
        pipeline_link = "/dashboard/pipeline/" + str(pipeline.id) + "/"
        self.assertNotContains(response, pipeline.mesa_sha)
        self.assertNotContains(response, pipeline_link)
        self.assertNotContains(response, ci.pipeline_url(pipeline.id))

    def setUp(self):
        self.fake_ci = FakeCI()

        self.fake_ci.add_job(13, "success", {"trace1": "match", "trace2": "match"})
        self.fake_ci.add_job(14, "success", {"trace1": "match", "trace2": "differ"})
        self.fake_ci.add_job(15, "success", {"trace1": "match", "trace2": "match"})

        self.fake_ci.add_pipeline(1, "success", 13, True)
        self.fake_ci.add_pipeline(2, "failed", 14, True)
        self.fake_ci.add_pipeline(3, "failed", None, False)
        self.fake_ci.add_pipeline(4, "success", 15, False)

        self.client = Client()

    def test_db_is_populated_from_ci(self):
        self.client.get('/dashboard/')

        for p in self.fake_ci.pipelines:
            pr = PipelineResult.objects.get(id=p.id)
            self.assertEquals(pr.mesa_sha, p.mesa_sha)
            self.assertEquals(pr.triggered_by_mesa_push, p.triggered_by_mesa_push)
            if p.status == "success":
                self.assertEquals(pr.status, "pass")
            elif p.status == "failed":
                self.assertEquals(pr.status, "fail")
            else:
                self.assertEquals(pr.status, "unknown")

    def test_landing_contains_only_pipelines_triggered_by_mesa(self):
        response = self.client.get('/dashboard/')

        for p in self.fake_ci.pipelines:
            if p.triggered_by_mesa_push:
                self._assertLandingContainsPipeline(response, p)
            else:
                self._assertLandingNotContainsPipeline(response, p)

    def test_landing_contains_only_finished_pipelines(self):
        self.fake_ci.add_pipeline(5, "running", None, False)

        response = self.client.get('/dashboard/')

        p = self.fake_ci.get_pipeline(5)
        self._assertLandingNotContainsPipeline(response, p)

    def test_landing_contains_pipeline_status(self):
        shown = [p for p in self.fake_ci.pipelines if p.triggered_by_mesa_push]
        count_pass = len([p for p in shown if p.status == "success"])
        count_fail = len([p for p in shown if p.status == "failed"])

        response = self.client.get('/dashboard/')

        self.assertContains(response, "pass", count=count_pass)
        self.assertContains(response, "fail", count=count_fail)

    def test_pipeline_view_contains_trace_names_and_links(self):
        response = self.client.get('/dashboard/pipeline/1/')
        pipeline = self.fake_ci.get_pipeline(1)
        job = self.fake_ci.get_test_job(pipeline.test_job_id)

        self.assertContains(response, "trace1")
        self.assertContains(response, ci.trace_summary_url(job.id, "trace1"))
        self.assertContains(response, "trace2")
        self.assertContains(response, ci.trace_summary_url(job.id, "trace2"))

    def test_pipeline_view_contains_trace_status(self):
        response = self.client.get('/dashboard/pipeline/1/')

        self.assertContains(response, "pass", count=2)
        self.assertNotContains(response, "fail")

        response = self.client.get('/dashboard/pipeline/2/')
        self.assertContains(response, "pass", count=1)
        self.assertContains(response, "fail", count=1)

    def test_pipeline_view_without_test_job_contains_no_trace_status(self):
        response = self.client.get('/dashboard/pipeline/3/')
        self.assertNotContains(response, "pass")
        self.assertNotContains(response, "fail")
