from django import template

register = template.Library()

@register.filter
def status_icon(status):
    status_attrs = {
        'pass': ('pass', 'black', 'lawngreen','&#10003;'),
        'fail': ('fail', 'black', 'red','&#10007;'),
        'unknown': ('unknown', 'black', 'lightgray', '?'),
        }

    attr = status_attrs.get(status, ('invalid', 'white', 'white', ''))
    span = '<span class="%s" style="display: inline-block; width: 25px; height: 25px; border-radius: 25px; font-size: 125%%; color: %s; background-color: %s">%s</span>' % attr

    return span
