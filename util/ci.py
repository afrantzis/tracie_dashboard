import gitlab
import re
from tracie_dashboard.settings import GITLAB_TOKEN

gl = None
tracie = None

GITLAB_URL = "http://gitlab.freedesktop.org"
PROJECT_NAME = "gfx-ci/tracie/tracie"

def _connect_to_ci():
    global gl
    global tracie
    if tracie is None:
        gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
        tracie = gl.projects.get(PROJECT_NAME)

def _get_job_trace_results(trace):
    results = {}

    result_pattern = re.compile("\[diff_trace_images\] Info: Images (\S+) for (\S+)")
    for l in trace.splitlines():
        m = result_pattern.match(l)
        if m is not None:
            results[m[2]] = m[1]

    return results

def _get_mesa_sha(job):
    try:
        return job.artifact("mesa_sha.txt").decode("utf-8").strip()
    except:
        return None

class Job:
    __slots__ = ('id', 'status', 'trace_results')
    def __init__(self):
        self.id = None
        self.status = None
        self.trace_results = {}

class Pipeline:
    __slots__ = ('id', 'status', 'mesa_sha', 'test_job_id', 'triggered_by_mesa_push')
    def __init__(self):
        self.id = None
        self.status = None
        self.mesa_sha = None
        self.test_job_id = None
        self.triggered_by_mesa_push = None

def get_pipeline_ids():
    _connect_to_ci()

    return [p.id for p in tracie.pipelines.list()]

def get_pipeline(pid):
    _connect_to_ci()

    p = tracie.pipelines.get(pid)
    vars = p.variables.list()
    mesa_push_trigger = next((v.value == "true" for v in vars if v.key == "MESA_PUSH_TRIGGER"), False)

    pipeline = Pipeline()
    pipeline.id = p.id
    pipeline.status = p.status
    pipeline.triggered_by_mesa_push = mesa_push_trigger

    for j in p.jobs.list():
        if j.stage == "build":
            job = tracie.jobs.get(j.id, lazy=True)
            pipeline.mesa_sha = _get_mesa_sha(job)
        elif j.stage == "test":
            pipeline.test_job_id = j.id

    return pipeline

def get_test_job(jid):
    _connect_to_ci()

    job = tracie.jobs.get(jid)
    trace = job.trace().decode("utf-8")

    test_job = Job()
    test_job.id = jid
    test_job.status = job.status
    test_job.trace_results = _get_job_trace_results(trace)

    return test_job

def project_url():
    return GITLAB_URL + "/" + PROJECT_NAME

def pipeline_url(pid):
    return GITLAB_URL + "/" + PROJECT_NAME + "/pipelines/" + str(pid)

def job_url(jid):
    return GITLAB_URL + "/" + PROJECT_NAME + "/-/jobs/" + str(jid)

def trace_summary_url(jid, name):
    return job_url(jid) + "/artifacts/file/results/" + name + "/summary.html"
